Karhukolari - A.I.A

Ei voi tietää mihin mennään
Tietämättä mistä ollaan tulossa
Se mitä olen ennestään
Määrittelee sen mitä olen huomenna

Antaisin itselleni anteeksi
Jos sitä ensin pyytäisin
Ja tämän katkeruuden 
Minä itsestäni poistaisin

Ei voi sietää pettymystä
Oltuaan täysin varma onnistumisesta
Ja nämä tunteet sua kohtaan
Ovat sisimpääni tatuoituna

Antaisin itselleni anteeksi
Jos sitä ensin pyytäisin
Ja tämän katkeruuden 
Minä itsestäni poistaisin

Ei voi tietää mihin mennään
Tietämättä mistä ollaan tulossa
Se mitä olen ennestään
Määrittelee sen mitä olen huomenna

Antaisin itselleni anteeksi
Jos sitä ensin pyytäisin
Jos voisin resetoida muistini
Parhaat palat säilyttäisin
Ja tämän katkeruuden 
Minä itsestäni poistaisin