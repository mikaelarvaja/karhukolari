
// When the user clicks on the button, open the modal
display = function(file) {
    var modal = document.getElementById('lyric_modal');
    modal.style.display = "block";
    readTextFile(file, 'lyric_content','lyric_text');
}

// When the user clicks on <span> (x), close the modal
hide = function() {
    var modal = document.getElementById('lyric_modal');
    modal.style.display = "none";
}

// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
    var modal = document.getElementById('lyric_modal');
    if (event.target == modal) {
        modal.style.display = "none";
    }
} 

showVideo = function(id) {
  var modal = document.getElementById(id);
  var display = modal.style.display;
  if (display == "none" || display == "") {
    modal.style.display = "block";
  }
  else {
    modal.style.display = "none";
  }
}

function readTextFile(file, elementId, className) {
  var rawFile = new XMLHttpRequest();
  rawFile.open("GET", file, false);
  rawFile.onreadystatechange = function() {
    if (rawFile.readyState === 4) {
      if (rawFile.status === 200 || rawFile.status == 0) {
        var allText = rawFile.responseText;
        allText = allText.replace(/(?:\r\n|\r|\n)/g, '<br>');
        console.log(allText);
        var modal = document.getElementById(elementId);
        modal.innerHTML = `<p class=${className}>${allText}</p>`;
      }
    }
  }    
  rawFile.send(null);
}

window.onload = function(event) {
  var coll = document.getElementsByClassName("collapsible");
  for (var i = 0; i < coll.length; i++) {
    coll[i].addEventListener("click", function() {
      this.classList.toggle("active");
      var content = this.nextElementSibling;
      if (content.style.display === "block") {
        content.style.display = "none";
      } else {
        content.style.display = "block";
      }
    });
  }
}