Karhukolarin verkkosivut by Mikael Arvaja

Karhukolari on tamperelainen rockyhtye, jossa soitan kitaraa ja laulan. 
Yliopistossa innostuin koodaamisesta ja päätin ensimmäisenä omana projektinani 
tehdä bändilleni verkkosivut. Sivuilta löytyy kuvia, julkaistu musiikki, 
tulevat keikat sekä yhteystiedot.

Sivut pyörivät osoitteessa http:/karhukolari.fi